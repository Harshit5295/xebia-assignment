import React, { Component } from "react";
import axios from "axios";

import "./Search.scss"
import endPointUrl from "../../configFiles/endPoint";

class SearchPage extends Component{
    state = {
        searchedKeyword : "",
        noResultError : null,
        planetData : []
    };

    componentDidMount() {
        this.fetchPlanetData();
    }

    handleSearch = (e) => {
        this.setState({
            searchedKeyword : e.target.value
        })
    };

    fetchPlanetData = () => {
        axios({
            method : "GET",
            url : endPointUrl.searchPlanets,
            headers : {
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                if(res.status === 200) {
                    this.setState({
                        planetData : res.data.results
                    }, () => {
                        axios.get(res.data.next)
                            .then((res) => {
                                let morePlanetData = [...this.state.planetData, ...res.data.results]
                                this.setState({
                                    planetData : morePlanetData
                                })
                            })
                    })
                }
            })
    };

    render() {
        return(
            <div className="search_page">
                <div className="search_box_wrapper">
                    <input
                        type="text"
                        placeholder="Search Planet"
                        value={this.state.searchedKeyword}
                        autoComplete="off"
                        onChange={this.handleSearch}
                    />
                    {
                        this.state.searchedKeyword.length >= 2 &&
                        <div className="suggestions_wrapper">
                            <ul>
                                {
                                    this.state.planetData.map((planet,i) => {
                                        if(planet.name.includes(this.state.searchedKeyword)) {
                                            return(
                                                <li
                                                    key={i}
                                                    style={{
                                                        "fontSize" : planet.population > 20000000 ? "22px" : "14px",
                                                        "listStyleType" : "none",
                                                        "margin" : "8px 0",
                                                        "backgroundColor" : "#FFF"
                                                    }}>
                                                    {planet.name}
                                                </li>
                                            )
                                        }
                                    })
                                }
                            </ul>
                        </div>
                    }
                </div>
            </div>
        )
    }
}

export default SearchPage;