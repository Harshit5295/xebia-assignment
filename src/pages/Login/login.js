import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import "./login.scss";
import endPointUrl from "../../configFiles/endPoint";
const axios = require('axios');

class Login extends Component {
    state = {
        loginData : {
            userName : "",
            password : ""
        },
        starWarCharacters: null,
        errorMessage : null,
        successMessage : null
    };

    componentDidMount() {
        this.fetchStarWarCharacters();
    }

    handleLoginInputValues = (e) => {
        let obj = {...this.state.loginData};
        obj[e.target.id] = e.target.value;
        this.setState({
            loginData : obj
        })
    };

    fetchStarWarCharacters = () => {
        axios({
            method : "GET",
            url : endPointUrl.fetchStarWarCharacters,
            headers : {
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                let charactersArray = []
                res.data.results.map((character) => {
                    return charactersArray.push({username : character.name.toLowerCase(), password : character.birth_year})
                })
                this.setState({
                    starWarCharacters : charactersArray
                })
            })
    };

    handleLoginSubmit = (e) => {
        e.preventDefault();
        const foundUser = this.state.starWarCharacters.find(char => char.username === this.state.loginData.userName.toLowerCase())
        if(!foundUser) {
            this.setState({
                errorMessage : "User Name not found. Please enter correct User Name."
            })
        }
        else if (foundUser.password !== this.state.loginData.password) {
            this.setState({
                errorMessage : "Please Enter Correct Password."
            })
        }
        else if(
            foundUser.username === this.state.loginData.userName &&
            foundUser.password === this.state.loginData.password
        ) {
            this.setState({
                errorMessage : null,
                successMessage : "Successfully logged in. You will be redirected to the search page."
            }, () => {
                setTimeout(() => {
                    this.setState({
                        successMessage : null
                    }, () => {
                        this.props.history.push("/search");
                    })
                },2000)
            })
        }
        else return null;
    };

    render() {
        return(
            <div className="login">
                <div className="login_form_container">
                    <form onSubmit={this.handleLoginSubmit}>
                        <p className="form_headline">Please enter your credentials to login.</p>
                        <input
                            className="form-group"
                            type="text"
                            placeholder="USERNAME"
                            id="userName"
                            onChange={this.handleLoginInputValues}
                            required={true}
                            autoComplete="off"
                            value={this.state.loginData.emailAddress}
                        />
                        <input
                            className="form-group"
                            type="password"
                            placeholder="PASSWORD"
                            id="password"
                            onChange={this.handleLoginInputValues}
                            required={true}
                            autoComplete="off"
                            value={this.state.loginData.password}
                        />
                        {this.state.successMessage && <p className="success_message">{this.state.successMessage}</p>}
                        {this.state.errorMessage && <p className="error_message">{this.state.errorMessage}</p>}
                        <button className="btn btn-primary login_button" type="submit">Log In</button>
                    </form>
                </div>
            </div>
        )
    }
}

export default withRouter(Login);