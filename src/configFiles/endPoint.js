let baseUrl = "https://swapi.dev/api/";

let apiConfig = {
    fetchStarWarCharacters : baseUrl + "people",
    searchPlanets : baseUrl + "planets/"
}

module.exports = apiConfig;