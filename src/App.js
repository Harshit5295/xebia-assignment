import React from 'react';
import { Route, Switch } from 'react-router-dom';

import './App.css';
import Header from "./components/Header/header";
import Login from "./pages/Login/login";
import SearchPage from "./pages/Search/Search";

function App() {
  return (
    <div className="App">
      <Header/>
      <Switch>
        <Route path="/" exact component={Login} />
          <Route path="/search" component={SearchPage} />
      </Switch>
    </div>
  );
}

export default App;
